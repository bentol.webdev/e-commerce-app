import { useContext, useState } from "react";
import { Form, Button, Container, Row, Col, Nav } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../Usercontext";
import LoadingModalContext from "../LoadingModalContext";

export default function Register() {
  const { user } = useContext(UserContext);
  const { modalDispatch } = useContext(LoadingModalContext);

  const navigate = useNavigate();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  let isActive =
    firstName !== "" &&
    lastName !== "" &&
    email !== "" &&
    mobileNumber !== "" &&
    mobileNumber.length >= 8 &&
    password1 !== "" &&
    password2 !== "" &&
    password1 === password2;

  function registerUser(e) {
    e.preventDefault();
    modalDispatch({ type: "show" });
    console.log("fetching checkEmail");
    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log("email exists: ", data);
        // If email exists
        if (data) {
          const err = new Error("Duplicate email found");
          err.message2 = "Please provide a different email";
          throw err;
        } else {
          return fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              password: password1,
              mobileNo: mobileNumber,
            }),
          });
        }
      })
      .then((res) => res.json())
      .then((registerSuccess) => {
        modalDispatch({ type: "hide" });
        if (registerSuccess) {
          Swal.fire({
            icon: "success",
            title: "Registration successful",
            text: "Welcome to Zuitt",
          });
          navigate("/login");
        }
      })
      .catch((err) => {
        modalDispatch({ type: "hide" });
        Swal.fire({
          icon: "error",
          title: err.message,
          text: err.message2 ? err.message2 : "",
        });
      });
  }

  return user.id ? (
    <Navigate to="/login" />
  ) : (
    <Container>
      <Row>
        <Col md={{ span: 6, offset: 3 }}>
          <Form onSubmit={(e) => registerUser(e)}>
            <h1 className="text-center mt-5">Register</h1>

            <Form.Group controlId="firstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter first name"
                value={firstName}
                onChange={(evt) => setFirstName(evt.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="lastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter last name"
                value={lastName}
                onChange={(evt) => setLastName(evt.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="userEmail">
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                type="email"
                placeholder="email@mail.com"
                value={email}
                onChange={(evt) => setEmail(evt.target.value)}
                required
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone.
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="mobileNumber">
              <Form.Label>Mobile Number</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Mobile Number"
                value={mobileNumber}
                onChange={(evt) => setMobileNumber(evt.target.value)}
                required
                minLength={8}
              />
            </Form.Group>

            <Form.Group controlId="password1">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter a password"
                value={password1}
                onChange={(evt) => setPassword1(evt.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="password2">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Confirm your password"
                value={password2}
                onChange={(evt) => setPassword2(evt.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="text-center">
              <Button
                disabled={!isActive}
                className=" mt-3 btn-lg"
                variant={isActive ? "primary" : "danger"}
                type="submit"
                id="submitBtn"
              >
                Submit
              </Button>
            </Form.Group>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
