import { useContext, useEffect, useReducer, useState } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import LoadingModalContext from "../LoadingModalContext";

export default function AdminUsers() {
  const { modalDispatch } = useContext(LoadingModalContext);
  const [users, usersDispatch] = useReducer(userReducer, []);
  useEffect(() => {
    modalDispatch({ type: "show" });
    fetch(`${process.env.REACT_APP_API_URL}/users/admin/getAllProfiles`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log("admin users data", data);
        usersDispatch({ type: "set_users", users: data });
        modalDispatch({ type: "hide" });
      });
  }, []);

  return (
    <Container fluid style={{ backgroundColor: "#f5f5f5", minHeight: "100vh" }}>
      <Row>
        <Col md={{ span: 8, offset: 2 }}>
          <h1 className="mt-5">All Users</h1>
          <Row className="bg-dark-subtle fs-5">
            <Col md={3}>
              <span>Email</span>
            </Col>
            <Col md={3}>
              <span>First</span>
            </Col>
            <Col md={2}>
              <span>Last</span>
            </Col>
            <Col md={1}>
              <span>IsAdmin</span>
            </Col>
            <Col></Col>
          </Row>
          {users.map((user) => (
            <UserEntry user={user} usersDispatch={usersDispatch} />
          ))}
        </Col>
      </Row>
    </Container>
  );
}

function UserEntry({ user, usersDispatch }) {
  const { modalDispatch } = useContext(LoadingModalContext);

  return (
    <Row className="pt-3" style={{ borderBottom: "1px solid grey" }}>
      <Col md={3}>
        <span>{user.email}</span>
      </Col>
      <Col md={3}>
        <span>{user.firstName}</span>
      </Col>
      <Col md={2}>
        <span>{user.lastName}</span>
      </Col>
      <Col md={1}>
        <span>{String(user.isAdmin)}</span>
      </Col>
      <Col>
        <Button
          variant={user.isAdmin ? "info" : "danger"}
          onClick={() => {
            modalDispatch({ type: "show" });
            toggleIsAdmin(user).then((data) => {
              usersDispatch({ type: "set_user", user: { ...data } });
              modalDispatch({ type: "hide" });
            });
          }}
        >
          {user.isAdmin ? "Set as User" : "Set as Admin"}
        </Button>
      </Col>
    </Row>
  );
}

function userReducer(users, action) {
  switch (action.type) {
    case "set_user": {
      return users.map((user) => {
        if (user._id === action.user._id) {
          return action.user;
        }
        return user;
      });
    }
    case "set_users": {
      return [...action.users];
    }
  }
}

async function toggleIsAdmin(user) {
  const data = await fetch(
    `${process.env.REACT_APP_API_URL}/users/admin/toggle-admin-access`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        email: user.email,
      }),
    }
  )
    .then((res) => res.json())
    .then((data) => {
      if (data) {
        return fetch(
          `${process.env.REACT_APP_API_URL}/users/admin/getProfile`,
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
            body: JSON.stringify({
              userId: user._id,
            }),
          }
        );
      }
    })
    .then((res) => res.json());

  return data;
}
