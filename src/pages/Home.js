import { useContext } from "react";
import { Navigate } from "react-router-dom";
import { NavLink } from "react-router-dom";
import UserContext from "../Usercontext";
import { Container, Nav, Carousel } from "react-bootstrap";

export default function Home() {
  const { user } = useContext(UserContext);
  console.log("home user", user);
  if (user.isAdmin) {
    return <Navigate to="/adminDashboard" />;
  }

  return (
    <Container fluid>
      <h1 className="text-center">Welcome to PC Bazaar</h1>
      {/* <div id='HEROsomething' style={{width:"100%", minHeight:"500px", backgroundColor:'teal'}}></div> */}
      <Carousel interval={3000} className="mt-5" fade data-bs-theme="dark">
        <Carousel.Item>
          <img
            className="d-block w-100 img-fluid"
            src="https://promotions.newegg.com/vga/23-0766/1920x660.png"
            alt="First slide"
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100 img-fluid"
            src="https://promotions.newegg.com/23-0867/1920x660.png"
            alt="Second slide"
          />
        </Carousel.Item>
        {/* "https://c1.neweggimages.com/ProductImageCompressAll1280/14-930-064-V07.jpg" */}
        <Carousel.Item>
          <img
            className="d-block w-100 img-fluid"
            src="https://promotions.newegg.com/gaming/23-0905/1920x660.png"
            alt="Third slide"
          />
        </Carousel.Item>
      </Carousel>
      <div className="d-flex justify-content-center mt-4">
        <div
          className="shadow rounded mx-3 p-3 d-flex flex-column align-items-center"
          style={{ width: "300px", height: "180px" }}
        >
          <h3>Hello Customer!</h3>
          <div className="mt-auto mb-4 d-flex text-center">
            <NavLink className="mx-2">
              <span className="fs-3">Your Account</span>
            </NavLink>
            <NavLink to="/orders" className="mx-2">
              <span className="fs-3">Your Orders</span>
            </NavLink>
          </div>
        </div>
        <div
          className="shadow rounded mx-3 fs-3 d-flex align-items-center text-center"
          style={{ width: "300px", height: "180px" }}
        >
          <div>
            <NavLink to="/products">See All of our latest products!</NavLink>
          </div>
        </div>
      </div>
    </Container>
  );
}
