import { useEffect } from "react";
import { Container, Button, Row, Col } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import { useNavigate } from "react-router-dom";

export default function AdminDashboard() {
  const navigate = useNavigate();

  if (!localStorage.getItem("token")) {
    navigate("/login");
    return;
  }

  return (
    <Container style={{ minHeight: "100vh" }}>
      <h1 className="text-center mt-3 ">Admin DashBoard</h1>
      <div className="text-center">
        <Button
          onClick={() => navigate("/admin/products")}
          className="py-auto mx-1"
          style={{ width: 200, height: 100, fontSize: 30 }}
        >
          Products
        </Button>
        <Button
          onClick={() => navigate("/admin/users")}
          className="mx-1"
          style={{
            width: 200,
            height: 100,
            fontSize: 30,
            backgroundColor: "#57cc99",
            borderColor: "#57cc99",
          }}
        >
          Users
        </Button>
        <Button
          onClick={() => navigate("/admin/orders")}
          className="mx-1"
          variant="info"
          style={{ width: 200, height: 100, fontSize: 30, color: "white" }}
        >
          Orders
        </Button>
      </div>
    </Container>
  );
}
