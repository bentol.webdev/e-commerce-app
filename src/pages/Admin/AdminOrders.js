import { useEffect, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";

export default function AdminOrders() {
  const [orders, setOrders] = useState([]);

  // Manipulate the orders array to get a new array of orders per user
  let allUsers = [...new Set(orders.map((order) => order.userId))];
  let userOrders = allUsers.map((user) => {
    return orders.filter((order) => order.userId === user);
  });
  console.log("userOrders", userOrders);

  useEffect(() => {
    // Navigate to Login page if user is not logged in
    if (!localStorage.getItem("token")) {
      console.log("navigating to login");
      navigate("/login");
      return;
    }
    fetch(`${process.env.REACT_APP_API_URL}/orders/admin/all-orders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log("admin orders useeffect data", data);
        setOrders([...data]);
      });
  }, []);
  return (
    <Container fluid style={{ backgroundColor: "#f5f5f5", minHeight: "100vh" }}>
      <Row>
        <Col md={{ span: 8, offset: 2 }}>
          <h1 className="mt-5">All Orders</h1>
          {allUsers.map((user, userIndex) => (
            <UserOrders
              key={user}
              userId={user}
              orders={userOrders[userIndex]}
            />
          ))}
        </Col>
      </Row>
    </Container>
  );
}
// '/admin/getProfile'
function UserOrders({ userId, orders }) {
  const [userProfile, setUserProfile] = useState({});

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/admin/getProfile`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        userId: userId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setUserProfile(data);
      });
  }, []);

  return <h1>{userProfile.email}</h1>;
}
