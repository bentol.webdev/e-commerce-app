import { useContext, useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Carousel,
  Button,
  Form,
  Offcanvas,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";
import Cart from "../components/Cart";
import UserContext from "../Usercontext";
import CartContext from "../CartContext";
import LoadingModalContext from "../LoadingModalContext";

export default function ProductView() {
  const { productId } = useParams();
  const [product, setProduct] = useState({});
  const [quantity, setQuantity] = useState(1);
  const [reviews, setReviews] = useState([]);
  const [showCart, setShowCart] = useState(false);

  const { user, userDispatch } = useContext(UserContext);
  const { showCartDispatch } = useContext(CartContext);
  const { modalDispatch } = useContext(LoadingModalContext);

  useEffect(() => {
    modalDispatch({ type: "show" });
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log("productview", data);
        setProduct(data);
        modalDispatch({ type: "hide" });
      });

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/reviews`)
      .then((res) => res.json())
      .then((data) => {
        console.log("productview fetch review", data);
        setReviews(data);
      });
  }, []);

  return (
    <div className="d-flex">
      <Container fluid>
        <Row>
          <Col md={{ span: 4, offset: 4 }} className="d-flex flex-column">
            <Carousel
              interval={3000}
              className="mt-5"
              fade
              data-bs-theme="dark"
            >
              <Carousel.Item>
                <img
                  className="d-block w-100 img-fluid"
                  src={product.images && product.images[0]}
                  alt="First slide"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100 img-fluid"
                  src={product.images && product.images[1]}
                  alt="Second slide"
                />
              </Carousel.Item>
              {/* "https://c1.neweggimages.com/ProductImageCompressAll1280/14-930-064-V07.jpg" */}
              <Carousel.Item>
                <img
                  className="d-block w-100 img-fluid"
                  src={product.images && product.images[2]}
                  alt="Third slide"
                />
              </Carousel.Item>
            </Carousel>
            <div className="mt-5">
              <h3>{product.name}</h3>
              {product.averageRating ? (
                <p className="fs-5">{`Rating: ${product.averageRating}`}</p>
              ) : (
                ""
              )}
              <p>
                {product.stockQuantity > 0 ? (
                  <span style={{ fontWeight: "bold" }}>
                    In Stock. {product.stockQuantity} items available
                  </span>
                ) : (
                  <span style={{ fontWeight: "bold", color: "red" }}>
                    Sold Out
                  </span>
                )}
              </p>
              <div className="mb-3">
                <Link className="">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="currentColor"
                    className="bi bi-heart"
                    viewBox="0 0 16 16"
                  >
                    <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z" />
                  </svg>{" "}
                  Add to Wishlist
                </Link>
              </div>

              <p className="fs-1">${product.price}</p>

              <Form.Group className="w-25 mb-3">
                <Form.Label>Quantity</Form.Label>
                <Form.Control
                  value={quantity}
                  onChange={(e) => setQuantity(e.target.value)}
                  min={1}
                  max={product.stockQuantity}
                  type="number"
                />
              </Form.Group>
              <div className="d-grid ">
                <Button
                  disabled={quantity > product.stockQuantity || !user.id}
                  onClick={() => {
                    addToCartFetch().then((data) => {
                      userDispatch({
                        type: "set_user_cart",
                        cartItems: data.cartItems,
                        totalAmountPayable: data.totalAmountPayable,
                      });
                      showCartDispatch({ type: "show_cart" });
                    });
                  }}
                  className="btn-lg py-4"
                  style={{ backgroundColor: "#ffa33a", border: "none" }}
                >
                  Add to Cart{" "}
                </Button>
                {user.id ? (
                  ""
                ) : (
                  <Link to="/login">Already have an account? Sign in</Link>
                )}
              </div>
            </div>
            <div
              style={{ marginTop: "5rem", border: "1px solid #eee" }}
              id="reviews"
            >
              <h3>Reviews</h3>
              {/* {reviews.map(review => <Review review={review}/>)} */}
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );

  // This function will make a post fetch request to add a product to user's shoppingCart,
  // it will also retrieve the updated profile of the user along with the shoppingCart
  async function addToCartFetch() {
    const data = await fetch(
      `${process.env.REACT_APP_API_URL}/users/user/cart/`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({ productId, quantity }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log("addToCart fetch success", data);

        if (data) {
          return fetch(`${process.env.REACT_APP_API_URL}/users/user/profile`, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          });
        }
      })
      .then((res) => res.json());

    console.log("fetch profile after add to cart", data);
    return data;
  }
}
