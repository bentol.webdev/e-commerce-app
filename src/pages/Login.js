import { useContext, useState } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../Usercontext";
import LoadingModalContext from "../LoadingModalContext";

export default function Login() {
  const { user, userDispatch } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { modalDispatch } = useContext(LoadingModalContext);
  let isActive = email !== "" && password !== "";

  function loginUser(e) {
    e.preventDefault();
    console.log("fetching login");
    modalDispatch({ type: "show" });
    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.access !== "undefined") {
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
            icon: "success",
            title: "Login successful",
            text: "Welcome to PC Bazaar!",
          });
        } else {
          Swal.fire({
            icon: "error",
            title: "Authentication failed",
            text: "Please check your login credentials and try again.",
            footer: '<a href="">Why do I have this issue?</a>',
          });
        }
        modalDispatch({ type: "hide" });
      });
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/user/profile`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log("login ", data);

        userDispatch({
          type: "set_user",
          user: data,
        });
      });
  };

  return user.id ? (
    user.isAdmin ? (
      <Navigate to="/adminDashboard" />
    ) : (
      <Navigate to="/" />
    )
  ) : (
    <Container>
      <Row>
        <Col md={{ span: 6, offset: 3 }}>
          <Form className="text-center" onSubmit={(e) => loginUser(e)}>
            <h1 className="text-center mt-5">Login</h1>
            <Form.Group className="text-start mt-3" controlId="userEmail">
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="text-start mt-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </Form.Group>
            <Button
              disabled={!isActive}
              className="mt-5"
              size="lg"
              variant="success"
              type="submit"
              id="submitBtn"
            >
              Login
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
