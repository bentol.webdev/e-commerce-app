import { createContext, useReducer } from "react";

const LoadingModalContext = createContext(null)

export default LoadingModalContext
export const LoadingModalProvider = LoadingModalContext.Provider

