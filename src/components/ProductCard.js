import { useContext } from "react";
import { Button, Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../Usercontext";
import CartContext from "../CartContext";
import LoadingModalContext from "../LoadingModalContext";

export default function ProductCard({ product }) {
  const { user, userDispatch } = useContext(UserContext);
  const { showCartDispatch } = useContext(CartContext);
  const { modalDispatch } = useContext(LoadingModalContext);

  const navigate = useNavigate();

  return (
    <Card className="m-1 shadow-sm" style={{ width: "22rem" }}>
      <Card.Img
        as="img"
        className="img-fluid"
        variant="top"
        src={product.images && product.images[0]}
        style={{ width: "400px", height: "250px" }}
      />
      <Card.Body>
        <Card.Subtitle> &#9733;&#9733;&#9733;&#9733;&#9734; 4.5 </Card.Subtitle>
        <Card.Title>{product.name}</Card.Title>
        <Card.Title className="fs-3">${product.price}</Card.Title>
        <Card.Text>{product.description}</Card.Text>
        <div className="d-flex justify-content-around">
          {user.id ? (
            <Button
              style={{ backgroundColor: "#ffa33a", border: "none" }}
              className="shadow"
              onClick={() => {
                modalDispatch({ type: "show" });
                addToCartFetch(product._id).then((data) => {
                  userDispatch({
                    type: "set_user_cart",
                    cartItems: data.cartItems,
                    totalAmountPayable: data.totalAmountPayable,
                  });

                  modalDispatch({ type: "hide" });
                  Swal.fire({
                    icon: "success",
                    title: "Add to cart successful",
                    text: `Added ${product.name} to cart`,
                  });

                  showCartDispatch({ type: "show_cart" });
                });
              }}
            >
              Add to Cart
            </Button>
          ) : (
            ""
          )}
          <Button
            className="shadow"
            variant="info"
            onClick={() => navigate(`/products/${product._id}`)}
          >
            View Details
          </Button>
        </div>
      </Card.Body>
    </Card>
  );

  async function addToCartFetch(productId) {
    console.log("products add to cart fetch", productId);
    const data = await fetch(
      `${process.env.REACT_APP_API_URL}/users/user/cart/`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({ productId: productId, quantity: 1 }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log("addToCart fetch success", data);

        if (data) {
          return fetch(`${process.env.REACT_APP_API_URL}/users/user/profile`, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          });
        }
      })
      .then((res) => res.json());

    console.log("fetch profile after add to cart", data);
    return data;
  }
}
