import "./LoadingModal.css";
export default function LoadingModal({ hide }) {
  return <div id="loading-modal" className="modal loading"></div>;
}
