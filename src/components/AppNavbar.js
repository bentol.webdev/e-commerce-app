import { useContext } from "react";
import { NavLink } from "react-router-dom";
import { Container, Nav, Navbar } from "react-bootstrap";
import UserContext from "../Usercontext";
import Cart from "./Cart";
import CartContext from "../CartContext";
import LoadingModal from "./LoadingModal";
import LoadingModalContext from "../LoadingModalContext";

export default function AppNavbar() {
  const { user } = useContext(UserContext);
  const { showCartDispatch } = useContext(CartContext);
  const { showLoadingModal, modalDispatch } = useContext(LoadingModalContext);

  if (showLoadingModal) {
    document.querySelector("body").classList.add("loading");
  } else {
    document.querySelector("body").classList.remove("loading");
  }

  console.log("navbar user", user);
  return (
    <>
      <Navbar
        style={{ minHeight: "5rem" }}
        sticky="top"
        id="appNavbar"
        bg="light"
        expand="lg"
        className="shadow"
      >
        <Container className="px-5" fluid>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Brand
            as={NavLink}
            to={"/"}
            className="ps-3 me-auto fw-bold fs-3"
          >
            PC BAZAAR
          </Navbar.Brand>
          <Nav className="ms-auto">
            {user.id ? (
              <>
                <Nav.Link className="fs-4" as={NavLink} to="/logout">
                  Logout
                </Nav.Link>

                {/* Shopping Cart Icon */}
                <Nav.Link
                  onClick={() => showCartDispatch({ type: "show_cart" })}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="40"
                    height=""
                    fill="currentColor"
                    className="bi bi-cart3"
                    viewBox="0 0 16 16"
                  >
                    {" "}
                    <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                  </svg>
                </Nav.Link>
              </>
            ) : (
              <>
                <Nav.Link className="fs-4" as={NavLink} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link className="fs-4" as={NavLink} to="/register">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Container>
        <Cart />
      </Navbar>
      <LoadingModal />
    </>
  );
}
