import { useContext, useEffect, useState } from "react";
import { ListGroup, Offcanvas, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import UserContext from "../Usercontext";
import CartContext from "../CartContext";

export default function Cart() {
  const { user, userDispatch } = useContext(UserContext);
  const { showCart, showCartDispatch } = useContext(CartContext);
  const navigate = useNavigate();

  console.log("cart user", user);
  return (
    <Offcanvas
      placement="end"
      show={showCart}
      onHide={() => showCartDispatch({ type: "hide_cart" })}
    >
      <Offcanvas.Header closeButton>
        <Offcanvas.Title>Shopping Cart</Offcanvas.Title>
      </Offcanvas.Header>
      <Offcanvas.Body>
        <ListGroup className="">
          {user.cartItems.map((item) => (
            <CartEntry
              key={item.productId}
              entry={item}
              userDispatch={userDispatch}
            />
          ))}
        </ListGroup>
        <div className="mt-3 text-end">
          <span className="fs-3">
            Total: ${user.totalAmountPayable.toFixed(2)}
          </span>
        </div>
        <div className="mt-3 d-flex justify-content-around">
          <Button
            disabled={user.cartItems.length === 0}
            className="btn-lg px-4 pt-2"
          >
            View Cart
          </Button>
          <Button
            disabled={user.cartItems.length === 0}
            onClick={() => {
              showCartDispatch({ type: "hide_cart" });
              navigate("/checkout");
            }}
            className="btn-lg px-4 pt-2"
            style={{ backgroundColor: "#ffa33a", border: "none" }}
          >
            Checkout
          </Button>
        </div>
      </Offcanvas.Body>
    </Offcanvas>
  );
}

function CartEntry({ entry, userDispatch }) {
  const [product, setProduct] = useState({});

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${entry.productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log("cartEntry data", data);
        setProduct(data);
      });
  }, []);

  return (
    <ListGroup.Item>
      <div className="d-flex ">
        <div className="">
          <div>{product.name}</div>
          <div>
            <span>Qty: {entry.quantity}</span>
          </div>
        </div>
        <div className="ms-auto">
          <span className="fs-4">${entry.subtotalAmount}</span>
        </div>
      </div>
      <div className="text-end">
        <Button
          className="btn-sm shadow border mt-2"
          variant=""
          onClick={() => {
            console.log("remove item");
            removeItem(product._id).then((user) => {
              userDispatch({
                type: "set_user_cart",
                cartItems: user.cartItems,
                totalAmountPayable: user.totalAmountPayable,
              });
            });
          }}
        >
          Remove
        </Button>
      </div>
    </ListGroup.Item>
  );
}

// remove item from cart and then fetch the updated user profile
async function removeItem(productId) {
  const data = await fetch(
    `${process.env.REACT_APP_API_URL}/users/user/cart/remove-product`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productId: productId,
      }),
    }
  )
    .then((res) => res.json())
    .then((data) => {
      if (data) {
        return fetch(`${process.env.REACT_APP_API_URL}/users/user/profile`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });
      }
    })
    .then((res) => res.json());

  console.log("remove item from cart data", data);
  return data;
}
